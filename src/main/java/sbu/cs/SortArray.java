package sbu.cs;

public class SortArray {

    /**
     * sort array arr with selection sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] selectionSort(int[] arr, int size) {

        for (int i = 0; i < size - 1; i++) {
            int min = i;
            for (int j = i + 1; j < size; j++)
                if (arr[j] < arr[min])
                    min = j;

            int temp = arr[min];
            arr[min] = arr[i];
            arr[i] = temp;
        }

        return arr;
    }

    /**
     * sort array arr with insertion sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] insertionSort(int[] arr, int size) {

        for (int i = 1; i < size; i++) {
            int key = arr[i];
            int j = i - 1;

            while (j >= 0 && arr[j] > key) {
                arr[j + 1] = arr[j];
                j = j - 1;
            }
            arr[j + 1] = key;
        }

        return arr;
    }

    /**
     * sort array arr with merge sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] mergeSort(int[] arr, int size) {

        arr = sort(arr, 0, size - 1);

        return arr;
    }

    public int[] sort(int[] arr, int l, int r) {

        if (l < r) {
            int m =l+ (r-l)/2;

            sort(arr, l, m);
            sort(arr, m + 1, r);

            merge(arr, l, m, r);
        }

        return arr;
    }

    public void merge(int[] arr, int l, int m, int r) {

        int n1 = m - l + 1;
        int n2 = r - m;

        int[] L = new int[n1];
        int[] R = new int[n2];

        System.arraycopy(arr, l, L, 0, n1);

        System.arraycopy(arr, (m + 1), R, 0, n2);


        int i = 0;
        int j = 0;

        int k = l;
        while (i < n1 && j < n2) {
            if (L[i] <= R[j]) {
                arr[k] = L[i];
                i++;
            }
            else {
                arr[k] = R[j];
                j++;
            }
            k++;
        }

        while (i < n1) {
            arr[k] = L[i];
            i++;
            k++;
        }

        while (j < n2) {
            arr[k] = R[j];
            j++;
            k++;
        }
    }

    /**
     * return position of given value in array arr which is sorted in ascending order.
     * use binary search algorithm and implement it in iterative form
     *
     * @param arr   sorted array
     * @param value value to be find
     * @return position of value in arr. -1 if not evalueists
     */
    public int binarySearch(int[] arr, int value) {

        int l = 0;
        int r = arr.length - 1;

        while (l <= r) {
            int m = (l + r) / 2;

            if (arr[m] == value)
                return m;
            if (arr[m] < value)
                l = m + 1;
            else
                r = m - 1;

        }

        return -1;
    }

    /**
     * return position of given value in array arr which is sorted in ascending order.
     * use binary search algorithm and implement it in recursive form
     *
     * @param arr   sorted array
     * @param value value to be find
     * @return position of value in arr. -1 if not evalueists
     */
    public int binarySearchRecursive(int[] arr, int value) {

        return recursiveSearch(arr, 0, arr.length - 1, value);
    }

    public int recursiveSearch(int[] arr, int l, int r, int value) {

        if (r >= l) {
            int m = l + (r - l) / 2;

            if (arr[m] == value)
                return m;

            if (arr[m] > value)
                return recursiveSearch(arr, l, m - 1, value);

            return recursiveSearch(arr, m + 1, r, value);
        }

        return -1;
    }
}

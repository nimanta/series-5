package sbu.cs;

import sbu.cs.functions.*;
import sbu.cs.squares.*;

public class App {

    public sbu.cs.functions.BlackFunction[] blackFunction = new BlackFunction[6];
    public sbu.cs.functions.WhiteFunction[] whiteFunction = new WhiteFunction[6];

    public void functionInitializer() {
        blackFunction[0] = null;
        whiteFunction[0] = null;
        blackFunction[1] = new BlackFunction1();
        whiteFunction[1] = new WhiteFunction1();
        blackFunction[2] = new BlackFunction2();
        whiteFunction[2] = new WhiteFunction2();
        blackFunction[3] = new BlackFunction3();
        whiteFunction[3] = new WhiteFunction3();
        blackFunction[4] = new BlackFunction4();
        whiteFunction[4] = new WhiteFunction4();
        blackFunction[5] = new BlackFunction5();
        whiteFunction[5] = new WhiteFunction5();
    }

    /**
     * use this function for magical machine question.
     *
     * @param n     size of machine
     * @param arr   an array in size n * n
     * @param input the input string
     * @return the output string of machine
     */

    public String main(int n, int[][] arr, String input) {

        functionInitializer();

        sbu.cs.squares.Green green = new Green(blackFunction);
        sbu.cs.squares.Yellow yellow = new Yellow(blackFunction);
        sbu.cs.squares.Blue blue = new Blue(blackFunction);
        sbu.cs.squares.Pink pink = new Pink(whiteFunction);

        green.walkRight(n, arr, input);
        green.walkDown(n, arr, input);

        yellow.walkRight(n, arr, green.getFirstRow());
        yellow.walkDown(n, arr, green.getFirstColumn());

        blue.walkRight(n, arr, green.column);
        blue.walkDown(n, arr, green.row);

        pink.walkDown(blue.getLastIndexOfRows(), yellow.upRight, n, arr);
        pink.walkRight(yellow.downLeft, blue.getLastIndexOfColumns(), n, arr);
        return pink.finalResult(arr[n - 1][n  -1]);
    }


}


package sbu.cs.squares;

import sbu.cs.functions.BlackFunction;

public class Yellow extends Square {

    public BlackFunction[] blackFunction;
    public String upRight;
    public String downLeft;

    public Yellow(BlackFunction[] blackFunction) {

        this.blackFunction = blackFunction;
    }

    @Override
    public void walkRight(int n, int[][] arr, String input) {

        this.upRight = blackFunction[arr[0][n - 1]].function(input);
    }

    @Override
    public void walkDown(int n, int[][] arr, String input) {

        this.downLeft = blackFunction[arr[n - 1][0]].function(input);
    }

}

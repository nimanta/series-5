package sbu.cs.squares;

import sbu.cs.functions.BlackFunction;

import java.util.Arrays;

public class Green extends Square {

    public BlackFunction[] blackFunction;

    public int firstRow_Right = 0;
    public int firstColumn_Down = 0;

    public String[] row;
    public String[] column;

    public Green(BlackFunction[] blackFunction) {

        this.blackFunction = blackFunction;
        System.out.println(Arrays.asList(this.blackFunction));
    }

    @Override
    public void walkRight(int n, int[][] arr, String input) {

        this.row = new String[n - 1];

        for (int i = 0; i < n - 1; i++) {
            this.row[i] = this.blackFunction[arr[firstRow_Right][i]].function(input);
            input = this.row[i];
        }
    }

    @Override
    public void walkDown(int n, int[][] arr, String input) {

        this.column = new String[n - 1];

        for (int i = 0; i < n - 1; i++) {
            this.column[i] = this.blackFunction[arr[i][firstColumn_Down]].function(input);
            input = this.column[i];
        }
    }

    public String getFirstRow() {

        return row[row.length - 1];
    }

    public String getFirstColumn() {

        return column[column.length - 1];
    }
}

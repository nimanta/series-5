package sbu.cs.squares;

import sbu.cs.functions.BlackFunction;

public class Blue extends Square {

    public BlackFunction[] blackFunction;
    public String[][] row_Info;
    public String[][] column_Info;

    public Blue(BlackFunction[] blackFunction) {

        this.blackFunction = blackFunction;
    }

    @Override
    public void walkRight(int n, int[][] arr, String[] input) {

        this.row_Info = new String[n - 2][n - 2];

        for (int i = 1; i < n - 1; i++)
            for (int j = 1; j < n - 1; j++) {
                this.row_Info[i - 1][j - 1] = blackFunction[arr[i][j]].function(input[i]);
                input[i] = this.row_Info[i - 1][j - 1];
            }
    }

    @Override
    public void walkDown(int n, int[][] arr, String[] input) {

        this.column_Info = new String[n - 2][n - 2];

        for (int j = 1; j < n - 1; j++)
            for (int i = 1; i < n - 1; i++) {
                this.column_Info[i - 1][j - 1] = blackFunction[arr[i][j]].function(input[j]);
                input[j] = this.column_Info[i - 1][j - 1];
            }
    }

    public String[] getLastIndexOfRows() {

        String[] rows = new String[this.row_Info.length];
        for (int i = 0; i < rows.length; i++)
            rows[i] = this.row_Info[i][row_Info.length - 1];

        return rows;
    }

    public String[] getLastIndexOfColumns() {

        String[] columns = new String[this.column_Info.length];
        System.arraycopy(this.column_Info[column_Info.length - 1], 0, columns, 0, columns.length);

        return columns;
    }


}

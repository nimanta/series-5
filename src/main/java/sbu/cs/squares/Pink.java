package sbu.cs.squares;

import sbu.cs.functions.WhiteFunction;

public class Pink extends Square {

    public WhiteFunction[] whiteFunction;
    public String up_Downward;
    public String left_Rightward;

    public Pink(WhiteFunction[] whiteFunction) {

        this.whiteFunction = whiteFunction;
    }

    @Override
    public void walkRight(String left, String[] up, int n, int[][] arr) {

        for (int j = 1; j < n - 1; j++) {
            this.left_Rightward = whiteFunction[arr[n - 1][j]].function(left, up[j - 1]);
            left = this.left_Rightward;
        }
    }

    @Override
    public void walkDown(String[] left, String up, int n, int[][] arr) {

        for (int i = 1; i < n - 1; i++) {
            this.up_Downward = whiteFunction[arr[i][n - 1]].function(left[i - 1], up);
            up = this.up_Downward;
        }
    }

    public String finalResult(int operation) {

        return whiteFunction[operation].function(this.left_Rightward, this.up_Downward);
    }
}

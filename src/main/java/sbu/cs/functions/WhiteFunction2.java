package sbu.cs.functions;

public class WhiteFunction2 implements WhiteFunction {
    @Override
    public String function(String arg1, String arg2) {

        StringBuilder sb2 = new StringBuilder(arg2);
        return arg1 + sb2.reverse();
    }
}

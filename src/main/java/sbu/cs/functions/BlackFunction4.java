package sbu.cs.functions;

public class BlackFunction4 implements BlackFunction {

    @Override
    public String function(String arg) {

        return arg.substring(arg.length() - 1) + arg.substring(0, arg.length() - 1);
    }
}

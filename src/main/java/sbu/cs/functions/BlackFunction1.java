package sbu.cs.functions;

public class BlackFunction1 implements BlackFunction {

    @Override
    public String function(String arg) {

        StringBuilder sb = new StringBuilder(arg);
        return sb.reverse().toString();
    }
}

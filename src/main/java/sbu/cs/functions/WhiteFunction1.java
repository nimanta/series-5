package sbu.cs.functions;

public class WhiteFunction1 implements WhiteFunction {

    @Override
    public String function(String arg1, String arg2) {

        StringBuilder sb1 = new StringBuilder(arg1);
        StringBuilder sb2 = new StringBuilder(arg2);
        StringBuilder result = new StringBuilder();

        while (!(sb1.isEmpty() || sb2.isEmpty())) {
            result.append(sb1.charAt(0));
            result.append(sb2.charAt(0));
            sb1.deleteCharAt(0);
            sb2.deleteCharAt(0);
        }

        if (sb1.isEmpty() && sb2.isEmpty())
            return result.toString();
        else if (sb1.isEmpty()) {
            result.append(sb2);
            return result.toString();
        }
        result.append(sb1);
        return result.toString();
    }
}

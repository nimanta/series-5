package sbu.cs.functions;

public class BlackFunction2 implements BlackFunction {

    @Override
    public String function(String arg) {

        StringBuilder result = new StringBuilder();
        for (char ch: arg.toCharArray()) {
            result.append(ch);
            result.append(ch);
        }

        return result.toString();
    }
}

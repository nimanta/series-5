package sbu.cs.functions;

public class BlackFunction5 implements BlackFunction {

    @Override
    public String function(String arg) {

        StringBuilder result = new StringBuilder();
        for (char ch: arg.toCharArray()) {
            result.append((char) ((int)'z' - ((int)ch - (int) 'a')));
        }

        return result.toString();
    }
}

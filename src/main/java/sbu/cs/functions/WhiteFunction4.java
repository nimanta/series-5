package sbu.cs.functions;

public class WhiteFunction4 implements WhiteFunction {

    @Override
    public String function(String arg1, String arg2) {

        if (arg1.length() % 2 == 0)
            return arg1;
        return arg2;
    }
}

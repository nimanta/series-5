package sbu.cs.functions;

public interface WhiteFunction {

    String function(String arg1, String arg2);
}
